# [1.1.0](https://gitlab.com/kluk/reshape/compare/v1.0.2...v1.1.0) (2023-07-16)


### Bug Fixes

* add missing character to material header ([047ef0a](https://gitlab.com/kluk/reshape/commit/047ef0ace453c4d870344cb16d441935c0c2c1c9))
* add missing db commit to /materials and /shapes ([b80ebfc](https://gitlab.com/kluk/reshape/commit/b80ebfcf0357a1235441b17147736b620410da17))
* endpoint authentication ([45242ef](https://gitlab.com/kluk/reshape/commit/45242ef36bae7a37c71532bddaa8001fdf9d2c0f))


### Features

* auto logout if api key expires ([53408b5](https://gitlab.com/kluk/reshape/commit/53408b55dd751a8287bcbb26d9054570b10073ef))
* auto remove expired sessions ([7501baf](https://gitlab.com/kluk/reshape/commit/7501bafca3ab3c6e0c99aa9ad852bc610247d08b)), closes [#5](https://gitlab.com/kluk/reshape/issues/5)
* display title and description of each shape ([ce252a4](https://gitlab.com/kluk/reshape/commit/ce252a49d4a911229c59ebe74e478a15dfeb8d46))
* improved mask to create a new material ([32e574b](https://gitlab.com/kluk/reshape/commit/32e574b3f7c2645b60d41023b46f7afe20bd9ec7))
* remember login session ([2acf99c](https://gitlab.com/kluk/reshape/commit/2acf99cfb65494f0b67d5d26c21cfd3b55a2faea)), closes [#4](https://gitlab.com/kluk/reshape/issues/4)
* update session timestamp ([52b4c13](https://gitlab.com/kluk/reshape/commit/52b4c13c88664422d049828627bd2075d327fd6d))
* verify stored api key with backend ([14832a9](https://gitlab.com/kluk/reshape/commit/14832a93c308ea57dbbdbc556e7da9f55c1b9749)), closes [#6](https://gitlab.com/kluk/reshape/issues/6)

## [1.0.2](https://gitlab.com/kluk/reshape/compare/v1.0.1...v1.0.2) (2023-07-06)


### Bug Fixes

* manual version bump ([33e28f9](https://gitlab.com/kluk/reshape/commit/33e28f93064db6fffcd574a0544c2f1425907e64))

## [1.0.1](https://gitlab.com/kluk/reshape/compare/v1.0.0...v1.0.1) (2023-07-03)


### Bug Fixes

* release versioned container ([c6b3eec](https://gitlab.com/kluk/reshape/commit/c6b3eecaf03f2988d3324a7b2ea7c5b17d820b9f))

# 1.0.0 (2023-07-02)


### Features

* add and remove materials ([9a3d70d](https://gitlab.com/kluk/reshape/commit/9a3d70ddb4489306f5ac2e297dda5e140bb585dd))
* add sqlx ([7b1ae95](https://gitlab.com/kluk/reshape/commit/7b1ae95a7e1c9274503fb20cbb3afcca974b64dc))
* add svelte app ([d608b54](https://gitlab.com/kluk/reshape/commit/d608b5429acc35e7d9de4418a5d4683d75f399d3))
* create rust project ([c8155e3](https://gitlab.com/kluk/reshape/commit/c8155e39b9a06fee8438813d8150ba33964f9508))
* create users with access levels ([9ecb5fa](https://gitlab.com/kluk/reshape/commit/9ecb5fa0c0797095d9d253025b77d135ae32f2ab))
* databae connection and docker ([24d7972](https://gitlab.com/kluk/reshape/commit/24d7972650296d6ccc274dda08f52b742ce2bcbf))
* delete user with admin tool ([0df32e1](https://gitlab.com/kluk/reshape/commit/0df32e1ead3cb1463cdeac5fc7b27917cc71da1e))
* enable semantic releases ([85ea5c8](https://gitlab.com/kluk/reshape/commit/85ea5c8abc242216889fef70932e69fb6e986d9c))
* first try loading materials ([0c489b0](https://gitlab.com/kluk/reshape/commit/0c489b020ccfdb6b8966f5ca18be8d9cd99e2870))
* implement update user ([184284c](https://gitlab.com/kluk/reshape/commit/184284cedb26d4a6e960dd5a1904b924e11dfa8f))
* improved material style ([7f8a385](https://gitlab.com/kluk/reshape/commit/7f8a38539449701f13ba9280a59df1a071699318))
* initial server setup ([53ccd48](https://gitlab.com/kluk/reshape/commit/53ccd4894b45019ee4fed73edef3688a4dec39ba))
* login screen without function ([ad86d78](https://gitlab.com/kluk/reshape/commit/ad86d78137565af7dd5d5ded073ec5200da258e2))
* setup mui material theme ([56d9669](https://gitlab.com/kluk/reshape/commit/56d9669b16b1d3cb11ea5086728c8533cd0b68b6))
* style materials and show content for levle ([886152a](https://gitlab.com/kluk/reshape/commit/886152a5c6c45208936590383906538b77e0ae56))
* working login authentication with apikey ([4e0d4e8](https://gitlab.com/kluk/reshape/commit/4e0d4e87016aea1b7d7eae1a4d937ca95397c4d3))
