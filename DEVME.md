# Development Guide

## Pre-requisites

- rust
- python

## Setup

Create a virtual python environment

```
python -m venv .venv
```

See [the official documentation](https://docs.python.org/3/tutorial/venv.html#creating-virtual-environments) on how to activate it on your platform.

Run these commands

```
rustup component add rustfmt
rustup component add clippy
cargo install sqlx-cli --no-default-features --features rustls,postgres
cargo install typeshare-cli
pip install -r requirements.txt
pre-commit install
```

## Docker

Develop locally with docker compose

```
cd docker
docker compose -p reshape up -d
```

Build and push a new version of the application

```
docker login
docker build -t riedeluk/reshape:latest .
docker push riedeluk/reshape:latest
```

## Database

Create an `.env` file with the database connection url that matches the settings
from the compose file.

```
DATABASE_URL=postgres://example:example@127.0.0.1:5432/reshape
```

Also see https://crates.io/crates/sqlx-cli

Create the database

```
sqlx database create
```

Create migrations

```
sqlx migrate add <name>
```

Apply migrations manually (this will be done automatically with `cargo run`)

```
sqlx migrate run
```

## Types

```
typeshare . --lang=typescript --output-file=reshape/src/reshape_types.ts
```

## CI

Build a new ci image if needed, the tag version should be incremented.

```
docker login registry.gitlab.com
docker build . -f ci/Dockerfile -t registry.gitlab.com/kluk/reshape:ci-1.0.0
docker push registry.gitlab.com/kluk/reshape:ci-1.0.0
```
