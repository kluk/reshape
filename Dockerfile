FROM rust:1.70.0 as backend-builder

COPY . /app
WORKDIR /app
ENV SQLX_OFFLINE=true
RUN cargo build --release
RUN cargo install typeshare-cli
RUN typeshare . --lang=typescript --output-file=/app/reshape/src/reshape_types.ts
RUN chmod +x /app/target/release/reshape

FROM node:20.3.1-slim as frontend-builder

COPY ./reshape /app
COPY --from=backend-builder /app/reshape/src/reshape_types.ts /app/src/reshape_types.ts

WORKDIR /app
RUN npm install
RUN npm run build

FROM alpine:latest as script-builder

RUN apk update && apk add dos2unix
COPY run.sh /app/run.sh
RUN chmod +x /app/run.sh
RUN dos2unix /app/run.sh

FROM nginx:stable-bullseye

COPY --from=backend-builder /app/target/release/reshape /app/reshape
COPY --from=backend-builder /app/target/release/reshape-admin /app/reshape-admin
COPY --from=frontend-builder /app/dist /usr/share/nginx/html
COPY --from=script-builder /app/run.sh /app/run.sh

EXPOSE 80/tcp

CMD [ "/bin/sh", "-c", "/app/run.sh" ]
