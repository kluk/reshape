# ReShape

Web based user interface to handle requested 3D print jobs.

## Development

See [the developer guidelines](DEVME)

## Deployment

An example deployment with docker compose is provided in `docker/example`

```
cd docker/example
docker compose -p reshape-deployment up -d --build
```

The reshape container provides access to reshape and its admin utility reshape-admin in the `/app` folder.

## User Management

This projects provides an admin utility called `reshape-admin`.

```
reshape-admin --help
```

### Create a new user

```
reshape-admin --create-user <username>
```

### Update an existing user

#### Password
```
reshape-admin update-user --name <username> --password <newPassword>
```

#### Access Level
```
reshape-admin update-user --name <username> --level <normal|admin>
```
