#!/bin/bash
if [[ -e "CI_NEW_VERSION" ]]; then
    CI_NEW_VERSION=`cat CI_NEW_VERSION`
    docker build -t riedeluk/reshape:$CI_NEW_VERSION -t riedeluk/reshape:stable .
    docker push --all-tags riedeluk/reshape
fi
