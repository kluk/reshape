CREATE TABLE users (
    name VARCHAR(320) NOT NULL PRIMARY KEY,
    hash VARCHAR(320) NOT NULL,
    salt VARCHAR(320) NOT NULL
);

CREATE TABLE materials (
    vendor varchar(320) NOT NULL,
    material varchar(320) NOT NULL,
    color varchar(320) NOT NULL,
    owner varchar(320) NOT NULL REFERENCES users(name) ON DELETE CASCADE,
    PRIMARY KEY (vendor, material, color)
);

CREATE TABLE access_level (
    user_name VARCHAR(320) NOT NULL PRIMARY KEY REFERENCES users(name) ON DELETE CASCADE,
    level VARCHAR(320) NOT NULL CHECK (level in ('normal', 'admin'))
);

CREATE TABLE sessions (
    user_name VARCHAR(320) NOT NULL REFERENCES users(name) ON DELETE CASCADE,
    api_key VARCHAR(320) NOT NULL,
    PRIMARY KEY (user_name, api_key)
)
