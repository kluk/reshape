CREATE TABLE shapes (
    title varchar(320) NOT NULL,
    description text,
    -- The file is hashed with SHA-512 and occupies 512 characters
    hash varchar(512) NOT NULL PRIMARY KEY,
    path varchar(1024) NOT NULL,
    public boolean NOT NULL,
    owner varchar(320) NOT NULL REFERENCES users(name) ON DELETE CASCADE
);
