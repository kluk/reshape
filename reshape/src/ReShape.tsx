import { createContext, useCallback, useEffect, useState } from "react";

import "./ReShape.scss";
import { Materials } from "./components/materials/materials";
import { Login } from "./components/login/login";
import { UserAccessLevel } from "./reshape_types";
import { Shapes } from "./components/shapes/shapes";
import { Session, SessionParser, getSessionCookie, removeSessionCookie, setSessionCookie } from "./session";


enum MainComponent {
  Materials = "Materials",
  Shapes = "Shapes",
  Requests = "Requests",
}

enum Theme {
  dark = "dark",
  light = "light",
}

export interface ApiResponse {
  json: any | undefined;
}

export interface AppContextType {
  session: Session,
  api: (response: Promise<Response>) => Promise<ApiResponse | undefined>,
}

const emptySession: Session = {
  api_key: "",
  user_name: "",
  access_level: UserAccessLevel.Normal,
};

// We avoid an optional session and instead create a default session.
// The context provider is only used once the Session is initialized properly
// thus making subcomponents less complex as they are only rendered if
// the have a valid session.
export const AppContext = createContext<AppContextType>({
  api: async () => { console.log("initial api func"); return undefined; },
  session: emptySession,
});

export function ReShape() {
  const [session, setSession] = useState<Session | undefined>(undefined);
  const [component, setComponent] = useState<MainComponent | undefined>(undefined);
  const [theme, setTheme] = useState<Theme>(Theme.dark);

  // Wrap calls to the api to catch the 440 error for every call to the api.
  // This enables us to invalidate the current session if it expired.
  const api = useCallback(async (promise: Promise<Response>) => {
    const response = await promise;
    if (response.status === 440) {
      console.error("Session expired, logging out");
      setSession(undefined);
      return undefined;
    }
    if (response.status !== 200) {
      return undefined;
    }
    try {
      return { json: await response.json() };
    } catch (_) {
      return { json: undefined };
    }
  }, []);

  // Read an existing session and try to use it again to stay logged in.
  // This effect will be executed exactly once because it is given an empty
  // list of dependencies.
  useEffect(() => {
    const parseResult = SessionParser.safeParse(getSessionCookie());
    if (!parseResult.success) {
      removeSessionCookie();
      return;
    }

    const verifyApiKey = async () => {
      if (!await api(fetch(
        `${globalThis.app.backendLocation}/login`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: parseResult.data.api_key,
      }
      )) === undefined) {
        console.error("The session has expired, please login again");
        removeSessionCookie();
      } else {
        setSession(parseResult.data);
      }
    };
    verifyApiKey().catch(console.error);

  }, [api]);

  // Store the updated session.
  // If the new session is undefined the user has logged out so the stored session is removed.
  useEffect(() => {
    if (session === undefined) {
      removeSessionCookie();
      return;
    }
    setSessionCookie(session);
  }, [session]);

  document.getElementsByTagName('html')[0].setAttribute('data-bs-theme', theme.toString());
  const nextTheme = theme === Theme.dark ? Theme.light : Theme.dark;
  const textColor = theme === Theme.dark ? "text-white" : "text-black";

  if (!session)
    return <AppContext.Provider value={{ api, session: emptySession }}>
      <Login newSession={setSession}></Login >
    </AppContext.Provider>;

  const components = (() => {
    switch (session.access_level) {
      case UserAccessLevel.Normal:
        return [
          MainComponent.Requests,
          MainComponent.Shapes,
        ];
      case UserAccessLevel.Admin:
        return [
          MainComponent.Requests,
          MainComponent.Materials,
          MainComponent.Shapes,
        ];
      default:
        return [];
    }
  })();

  if (component === undefined && components.length > 0) {
    setComponent(components[0]);
  }

  return <AppContext.Provider value={{ api, session }}>
    <div className="row reshape-sidebar-container">
      <div className={`reshape-sidebar col-auto d-flex flex-column flex-wrap flex-shrink-0 p-3 ${textColor}`}>
        <a href="/" className={`d-flex align-items-center mb-3 mb-md-0 me-md-auto text-decoration-none ${textColor}`}>
          <svg className="bi me-2" width="40" height="32"><use xlinkHref="#bootstrap"></use></svg>
          <span className="fs-4">ReShape</span>
        </a>
        <hr></hr>
        <ul className="nav nav-pills flex-column mb-auto">
          {components.map((comp: MainComponent) => {
            const name = comp.toString();
            return <li className="nav-item" onClick={() => setComponent(comp)} key={comp}>
              <a href="#" className={`nav-link ${component === comp ? "active" : ""} ${textColor}`} aria-current="page">
                <svg className="bi me-2" width="16" height="16"><use xlinkHref={name}></use></svg>
                {name}
              </a>
            </li>;
          })}
        </ul>
        <div className="d-flex justify-content-between">
          <a>Hello {session.user_name}!</a>
          <button className="btn btn-primary" onClick={() => {
            setComponent(undefined);
            setSession(undefined);
          }}>Bye</button>
        </div>
        <hr />
        <button className="btn btn-primary" onClick={() => {
          setTheme(nextTheme);
        }}>Enable {nextTheme} theme</button>
      </div>
      <div className="vr"></div>
      <div className="col py-3">
        {(() => {
          if (component === MainComponent.Materials)
            return <Materials></Materials>;
          if (component === MainComponent.Shapes)
            return <Shapes></Shapes>;
          return <></>;
        })()}
      </div>
    </div>
  </AppContext.Provider>;
}
