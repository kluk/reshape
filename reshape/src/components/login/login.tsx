import { useContext, useState } from "react";
import { PostLoginRequest, PostLoginResponse, UserAccessLevel } from "../../reshape_types";
import "./login.scss";
import { Session } from "../../session";
import { AppContext } from "../../ReShape";

interface Properties {
    newSession: (session: Session) => void;
};

export function Login({ newSession }: Properties) {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const context = useContext(AppContext);

    return <form className="text-center" onSubmit={async (event) => {
        event.preventDefault();
        const creds: PostLoginRequest = {
            user_name: username,
            password: password,
        };

        const response = await context.api(fetch(`${globalThis.app.backendLocation}/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(creds),
        }));

        if (response?.json === undefined) {
            console.error("Failed to authenticate with username and password");
            return;
        }
        const unpackedResponse = response.json as PostLoginResponse;
        newSession({
            user_name: username,
            api_key: unpackedResponse.api_key,
            // TODO improve conversion
            access_level: unpackedResponse.access_level === "admin" ? UserAccessLevel.Admin : UserAccessLevel.Normal,
        });
        console.log(`Authenticated user '${username}'`);

    }}>
        <h1>
            Welcome to ReShape
        </h1>

        <div className="form-outline mb-4">
            <input type="text" id="form2Example1" className="form-control text-center" onChange={(event) => {
                setUsername(event.target.value);
            }} />
            <label className="form-label" htmlFor="form2Example1">Username</label>
        </div>

        <div className="form-outline mb-4">
            <input type="password" id="password" className="form-control text-center" onChange={(event) => {
                setPassword(event.target.value);
            }} />
            <label className="form-label" htmlFor="password">Password</label>
        </div>
        <input type="submit" className="btn btn-primary btn-block mb-4" value="Sign in" />
    </form>;
}
