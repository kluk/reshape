import { useContext, useState } from "react";
import { PostMaterialsRequest } from "../../reshape_types";
import { AppContext, AppContextType } from "../../ReShape";
import Modal from 'react-modal';

enum Mode {
    Default,
    Create,
};
interface Properties {
    additionalClasses: string;
}

const root: HTMLElement | null = document.querySelector('#root');

export function CreateNewMaterial({ additionalClasses }: Properties) {
    const [mode, setMode] = useState<Mode>(Mode.Default);
    const [vendor, setVendor] = useState("");
    const [material, setMaterial] = useState("");
    const [color, setColor] = useState("");

    const context = useContext(AppContext);

    return [
        <div className={`card bg-secondary text-white d-flex justify-content-evenly ${additionalClasses}`} >
            <div className="d-flex justify-content-evenly"><button className="btn btn-primary" onClick={() => setMode(Mode.Create)}>New</button></div>
        </div>,
        (() => root === null ? <></> : <Modal
            isOpen={mode === Mode.Create}
            parentSelector={() => root}
            style={{
                // Center vertically and horizontally
                content: {
                    width: "350pt", height: "200pt",
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    margin: "auto"
                }
            }}
        >
            <div className="input-group mb-1">
                <span className="input-group-text">Vendor</span>
                <input type="text" className="form-control" placeholder="Vendor (e.g. Prusament)" onChange={
                    (event) => { setVendor(event.target.value) }
                } />
            </div>
            <div className="input-group mb-1">
                <span className="input-group-text">Material</span>
                <input type="text" className="form-control" placeholder="Material (e.g. PLA)" onChange={
                    (event) => { setMaterial(event.target.value) }
                } />
            </div>
            <div className="input-group mb-1">
                <span className="input-group-text">Color</span>
                <input type="text" className="form-control" placeholder="Color (e.g. red)" onChange={
                    (event) => { setColor(event.target.value) }
                } />
            </div>
            <div className="d-flex justify-content-evenly">
                <input type="submit" className="btn btn-primary btn-block mb-1" value="Create" onClick={async () => {
                    await create_material(context, vendor, material, color, "lukas", setMode);
                }} />
                <input type="submit" className="btn btn-primary btn-block mb-1" value="Cancel" onClick={() => { setMode(Mode.Default) }} />
            </div>
        </Modal>)()
    ];
}

async function create_material(
    context: AppContextType,
    vendor: string,
    material: string,
    color: string,
    owner: string,
    setMode: React.Dispatch<React.SetStateAction<Mode>>,
) {
    const body: PostMaterialsRequest = [{
        vendor: vendor, material: material, color: color, owner: owner
    }];

    if (!await context.api(fetch(
        `${globalThis.app.backendLocation}/materials?` +
        new URLSearchParams({ api_key: context.session.api_key }),
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body),
        }
    )) === undefined) {
        console.error("Failed to create new materials");
        return;
    }

    setMode(Mode.Default);
}
