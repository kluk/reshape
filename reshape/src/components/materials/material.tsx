import { useContext } from 'react';
import { AppContext, AppContextType } from '../../ReShape';
import { DeleteMaterialsRequest, Material as MaterialType } from '../../reshape_types';

interface Properties {
    additionalClasses: string;
    material: MaterialType;
};

export function Material({ additionalClasses, material }: Properties): JSX.Element {
    const context = useContext(AppContext);

    return <div className={`card bg-secondary text-white ${additionalClasses}`} key={`${material.vendor}${material.material}${material.color}`}>
        <div className='card-header d-flex justify-content-center'>{material.material} "{material.color}"</div>
        <div className='card-body'>
            <div>Vendor: {material.vendor}</div>
            <div>Material: {material.material}</div>
            <div>Color: {material.color}</div>
        </div>
        <div className='card-footer d-flex justify-content-center'>
            <button className="btn btn-primary mb-1" onClick={() => remove_material(context, material)}>Remove</button>
        </div>
    </div>;
}

async function remove_material(context: AppContextType, material: MaterialType) {
    const body: DeleteMaterialsRequest = [material];

    if (!await context.api(fetch(
        `${globalThis.app.backendLocation}/materials?` +
        new URLSearchParams({ api_key: context.session.api_key }),
        {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body),
        }
    )) === undefined) {
        console.error("Failed to create new materials");
        return;
    }
}
