
import { useContext, useEffect, useState } from 'react';
import { GetMaterialsResponse, Material as MaterialType } from '../../reshape_types';
import { Material } from './material';
import { CreateNewMaterial } from './create_new_material';
import { AppContext } from '../../ReShape';

const numColumns = 3;

export function Materials() {
    const [materials, setMaterials] = useState<MaterialType[]>([]);
    const context = useContext(AppContext);

    useEffect(() => {
        const interval = setInterval(async () => {
            const response = await context.api(fetch(
                `${globalThis.app.backendLocation}/materials?` +
                new URLSearchParams({ api_key: context.session.api_key }),
            ));
            if (response?.json !== undefined)
                setMaterials(response.json as GetMaterialsResponse);
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    return <div className="container"> {(() => {
        const numRows = Math.ceil((materials.length + 1) / numColumns);
        const rows = [];
        for (let row = 0; row < numRows; row++) {
            rows.push(<div className='row g-2'> {(() => {
                const columns = [];
                for (let column = 0; column < numColumns; column++) {
                    const materialIndex = row * numColumns + column;
                    const material: MaterialType | undefined = materials[materialIndex];
                    if (!material) {
                        columns.push(<CreateNewMaterial additionalClasses="col-3" key="create-new-material" />);
                        break;
                    }
                    columns.push(
                        <Material additionalClasses="col-3" material={material}></Material>
                    );
                }
                return columns;
            })()}
            </div>);
        }
        return rows;
    })()
    }
    </div>
}
