
import { useContext, useEffect, useState } from 'react';
import { GetShapesResponse, Shape as ShapeType } from '../../reshape_types';
import { AppContext } from '../../ReShape';

const numColumns = 3;

export function Shapes() {
    const [shapes, setShapes] = useState<ShapeType[]>([]);
    const context = useContext(AppContext);

    useEffect(() => {
        const interval = setInterval(async () => {
            const response = await context.api(fetch(
                `${globalThis.app.backendLocation}/shapes?` +
                new URLSearchParams({ api_key: context.session.api_key }),
            ));
            if (response?.json !== undefined)
                setShapes(response.json as GetShapesResponse);
        }, 250);
        return () => clearInterval(interval);
    }, []);

    return <div className="container"> {(() => {
        const numRows = Math.ceil((shapes.length + 1) / numColumns);
        const rows = [];
        for (let row = 0; row < numRows; row++) {
            rows.push(<div className='row g-2'> {(() => {
                const columns = [];
                for (let column = 0; column < numColumns; column++) {
                    const shapeIndex = row * numColumns + column;
                    const shape: ShapeType | undefined = shapes[shapeIndex];
                    if (!shape) {
                        // columns.push(<CreateNewMaterial additionalClasses="col-3" key="create-new-material" />);
                        break;
                    }
                    columns.push(
                        <div>{shape.title} {shape.description}</div>
                    );
                }
                return columns;
            })()}
            </div>);
        }
        return rows;
    })()
    }
    </div>
}
