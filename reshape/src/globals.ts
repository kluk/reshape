var app: globalAppVariables;

type globalAppVariables = {
  backendLocation: string;
};

globalThis.app = {
    backendLocation: (() => {
        const location = document.location;
        return `${location.protocol}//${location.hostname}:${location.port}/api`;
    })(),
};

Object.freeze(globalThis.app);
