import ReactDOM from 'react-dom/client'
import { ReShape } from './ReShape.tsx'
import './globals'
import './index.scss'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <ReShape />,
)
