import z from "zod";
import { UserAccessLevel } from "./reshape_types";

export const SessionParser = z.object({
    user_name: z.string(),
    api_key: z.string(),
    access_level: z.nativeEnum(UserAccessLevel),
});

export type Session = z.infer<typeof SessionParser>;

const sessionCookieyKey = "session";

export const removeSessionCookie = () => {
    localStorage.removeItem(sessionCookieyKey);
};

export const setSessionCookie = (session: Session) => {
    localStorage.setItem(sessionCookieyKey, JSON.stringify(session));
};

export const getSessionCookie = () => {
    const value = localStorage.getItem(sessionCookieyKey) ?? undefined;
    if (value === undefined) return undefined;
    try {
        return JSON.parse(value);
    } catch (_) {
        return undefined;
    }
};
