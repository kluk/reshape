#! /usr/bin/env python
"""
    Prepare everything for a release of the software.

    1. update the version number in `version`
    2. (Re)build the code to include the new version
    3. Generate packages

    The updated version is passed via the command line.
    This script expects to be called from the repository root.

    Usage: prepare_release.py <Major.Minor.Patch>

    The script is run by semantic-release in the CI pipeline.
"""

import sys
import subprocess
from subprocess import CompletedProcess
from typing import List, Optional

def do(args: List[str], cwd: Optional[str] = None):
    result: CompletedProcess = subprocess.run(args, cwd=cwd)
    if result.returncode != 0:
        sys.exit(result.returncode)

def main() -> int:
    if len(sys.argv) < 2:
        print(__doc__)
        return 1

    new_version: str = sys.argv[1]

    print("Bump frontend version to", new_version)
    do(["npm", "install"], cwd="reshape")
    do(["npm", "version", new_version], cwd="reshape")

    print("Bump backend version to", new_version)
    do(["cargo", "set-version", new_version])

    version_file = open("./CI_NEW_VERSION", "w")
    version_file.write(new_version)
    version_file.close()

    return 0


if __name__ == "__main__":
    sys.exit(main())
