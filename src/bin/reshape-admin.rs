use clap::{ArgGroup, Parser, Subcommand};
use color_eyre::eyre::{eyre, Context, Result};

use reshape::tables::User;
use sqlx::{postgres::PgPoolOptions, Pool, Postgres};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None, propagate_version = true)]
struct Args {
    #[clap(long)]
    create_user: Option<String>,

    #[clap(long)]
    delete_user: Option<String>,

    #[command(subcommand)]
    update_user: Option<UpdateUserArgs>,
}

#[derive(Subcommand, Debug)]
enum UpdateUserArgs {
    #[clap(
        group(ArgGroup::new("update-user").required(true)),
        group(ArgGroup::new("update-user-args").required(true).multiple(true))
    )]
    UpdateUser {
        #[clap(long, group = "update-user")]
        name: String,

        #[clap(long, group = "update-user-args")]
        level: Option<String>,

        #[clap(long, group = "update-user-args")]
        password: Option<String>,
    },
}

#[tokio::main]
async fn main() -> Result<()> {
    color_eyre::install()?;
    dotenv::dotenv().ok();

    let args = Args::parse();

    if let Some(user_name) = args.create_user {
        create_user(&user_name).await?;
    } else if let Some(user_name) = args.delete_user {
        delete_user(&user_name).await?;
    } else if let Some(args) = args.update_user {
        update_user(&args).await?;
    }

    Ok(())
}

async fn connect_to_db() -> Result<Pool<Postgres>> {
    let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set.");
    Ok(PgPoolOptions::new()
        .max_connections(1)
        .connect(&db_url)
        .await?)
}

struct PasswordHashedAndSalted {
    hash: String,
    salt: String,
}

fn compute_hash_salt(password: &String) -> Result<PasswordHashedAndSalted> {
    use argon2::Config;
    use rand::{distributions::Alphanumeric, Rng};

    if password.len() < 8 {
        return Err(eyre!(
            "The password provided must have at least 8 characters"
        ));
    }

    let random_generator = rand::thread_rng();

    let salt = random_generator
        .sample_iter(&Alphanumeric)
        .take(64)
        .map(char::from)
        .collect::<String>();

    let hash = argon2::hash_encoded(password.as_bytes(), salt.as_bytes(), &Config::default())
        .wrap_err("Failed to hash the password.")?;

    let Ok(true) = argon2::verify_encoded(&hash, password.as_bytes()) else {
        return Err(eyre!("Failed to verify the hashed password."));
    };

    Ok(PasswordHashedAndSalted { hash, salt })
}

async fn create_user(user_name: &String) -> Result<()> {
    let pool = connect_to_db().await?;

    let access_level = inquire::Select::new(
        "Please select the user access level",
        vec!["normal", "admin"],
    )
    .prompt()
    .wrap_err("Failed to inquire the user access level.")?;

    let password = inquire::Password::new("Enter a safe password")
        .with_display_mode(inquire::PasswordDisplayMode::Masked)
        .with_validator(inquire::validator::MinLengthValidator::new(8))
        .prompt()
        .wrap_err("Failed to inquire the user password.")?;

    let PasswordHashedAndSalted { hash, salt } = compute_hash_salt(&password)?;

    let new_user = sqlx::query_as!(
        User,
        "INSERT into users(name, hash, salt) values ($1, $2, $3) returning *",
        user_name.to_string(),
        hash,
        salt,
    )
    .fetch_one(&pool)
    .await?;

    sqlx::query!(
        "INSERT into access_level(user_name, level) values ($1, $2) returning *",
        user_name.to_string(),
        access_level,
    )
    .fetch_one(&pool)
    .await?;

    println!("Created user {}", new_user.name);

    Ok(())
}

async fn delete_user(user_name: &String) -> Result<()> {
    let pool = connect_to_db().await?;

    sqlx::query!("DELETE FROM users WHERE name=$1", user_name.to_string())
        .execute(&pool)
        .await?;

    println!("Removed user '{}' from database", user_name);

    Ok(())
}

async fn update_user(args: &UpdateUserArgs) -> Result<()> {
    let UpdateUserArgs::UpdateUser {
        name,
        level,
        password,
    } = args;

    let pool = connect_to_db().await?;

    if sqlx::query!("SELECT COUNT(*) FROM users WHERE name=$1", name)
        .fetch_one(&pool)
        .await?
        .count
        .unwrap_or(-1)
        != 1
    {
        return Err(eyre!("User '{}' does not exist", name));
    }

    if let Some(level) = level {
        sqlx::query!("UPDATE access_level SET level=$1", level)
            .execute(&pool)
            .await?;
        println!("Applied access level '{}' for '{}'", level, name);
    }

    if let Some(password) = password {
        let PasswordHashedAndSalted { hash, salt } = compute_hash_salt(password)?;
        sqlx::query!("UPDATE users SET hash=$1,salt=$2", hash, salt)
            .execute(&pool)
            .await?;
        println!("Applied new password for '{}'", name);
    }

    Ok(())
}
