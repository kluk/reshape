use axum::{
    routing::{get, post},
    Router,
};
use dotenv::dotenv;
use sqlx::{postgres::PgPoolOptions, Pool, Postgres};

use std::net::SocketAddr;
use tower_http::cors::{Any, CorsLayer};

use crate::routes::{
    login::{post_login, put_login},
    materials::{delete_materials, get_materials, post_materials},
    session::prune_api_keys,
    shapes::get_shapes,
};

mod routes;
mod tables;
mod types;

#[derive(Clone)]
pub struct ApplicationState {
    pool: Pool<Postgres>,
}

#[tokio::main]
async fn main() {
    dotenv().ok();
    let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set.");

    let state = ApplicationState {
        pool: PgPoolOptions::new()
            .max_connections(5)
            .connect(&db_url)
            .await
            .unwrap_or_else(|_| panic!("Failed to connect to \"{}\"", db_url)),
    };

    sqlx::migrate!("./migrations")
        .run(&state.pool)
        .await
        .expect("Failed to run migrations");

    let cors = CorsLayer::new().allow_origin(Any);

    let app = Router::new()
        .route("/login", post(post_login).put(put_login))
        .route(
            "/materials",
            get(get_materials)
                .post(post_materials)
                .delete(delete_materials),
        )
        .route("/shapes", get(get_shapes))
        .with_state(state.clone())
        .layer(cors);

    let addr = SocketAddr::from(([0, 0, 0, 0], 8000));
    println!("Listening on http://{}", addr);

    tokio::spawn(async move { prune_api_keys(state).await });
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .expect("Failed to start service");
}
