use std::str::FromStr;

use axum::http::StatusCode;
use serde::Deserialize;

use crate::{tables::UserAccessLevel, types::ApiKey};

use super::session::use_api_key;
#[derive(Debug, Deserialize)]
#[typeshare::typeshare]
pub struct AuthenticatedQuery {
    pub api_key: ApiKey,
}

pub async fn verify(
    transaction: &mut sqlx::Transaction<'_, sqlx::Postgres>,
    api_key: ApiKey,
    min_allowed_level: UserAccessLevel,
) -> Result<(), StatusCode> {
    // Touch the api key, if the api key does not exist this will result in an http
    // error that tells clients that their session has ended.
    use_api_key(transaction, &api_key).await?;

    let Ok(access_level) = sqlx::query_scalar!(
        "SELECT access_level.level FROM sessions JOIN users ON sessions.user_name = users.name JOIN access_level ON access_level.user_name = users.name WHERE sessions.api_key = $1",
        api_key
    )
        .fetch_one(&mut *transaction)
        .await else {
            return Err(StatusCode::FORBIDDEN);
        };
    let Ok(access_level) = UserAccessLevel::from_str(&access_level) else {
        return Err(StatusCode::FORBIDDEN);
    };

    // TODO improve logic if there are more than two levels but for now we can
    // just deny access if a normal user tries to access an admin endpoint.
    if min_allowed_level == UserAccessLevel::Admin && access_level == UserAccessLevel::Normal {
        println!("User not allowed !");
        return Err(StatusCode::FORBIDDEN);
    }

    Ok(())
}
