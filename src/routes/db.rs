use axum::http::StatusCode;

pub async fn commit(transaction: sqlx::Transaction<'_, sqlx::Postgres>) -> Result<(), StatusCode> {
    match transaction.commit().await {
        Ok(_) => Ok(()),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}
