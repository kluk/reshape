use argon2::Config;
use axum::{extract::State, http::StatusCode};
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use serde::{Deserialize, Serialize};
use typeshare::typeshare;

use crate::{
    tables::{Session, User},
    ApplicationState,
};

use super::{db::commit, session::use_api_key};

#[derive(Debug, Deserialize)]
#[typeshare]
pub struct PostLoginRequest {
    user_name: String,
    password: String,
}

#[derive(Debug, Serialize)]
#[typeshare]
pub struct PostLoginResponse {
    api_key: String,
    access_level: String,
}

pub async fn post_login(
    State(state): State<ApplicationState>,
    axum::Json(credentials): axum::Json<PostLoginRequest>,
) -> Result<axum::Json<PostLoginResponse>, StatusCode> {
    let Ok(mut transaction) = state.pool.begin().await else {
        return Err(StatusCode::INTERNAL_SERVER_ERROR);
    };

    let Ok(user) = sqlx::query_as!(
        User,
        "SELECT * FROM users WHERE name=$1",
        credentials.user_name,
    )
    .fetch_one(&mut transaction)
    .await else {
        return Err(StatusCode::UNAUTHORIZED);
    };

    let Ok(level) = sqlx::query!("SELECT level FROM access_level WHERE user_name=$1", user.name).fetch_one(&mut transaction).await else {
        return Err(StatusCode::UNAUTHORIZED);
    };
    let level = level.level;

    // If this user already has an active session we can reuse it
    if let Ok(session) = sqlx::query_as!(
        Session,
        "SELECT * FROM sessions WHERE user_name=$1",
        credentials.user_name
    )
    .fetch_one(&mut transaction)
    .await
    {
        // Use the key immediately to avoid pruning
        use_api_key(&mut transaction, &session.api_key).await?;
        commit(transaction).await?;
        return Ok(axum::Json(PostLoginResponse {
            api_key: session.api_key,
            access_level: level,
        }));
    };

    let Ok(hash_value) = argon2::hash_encoded(
        credentials.password.as_bytes(),
        user.salt.as_bytes(),
        &Config::default(),
    ) else {
        return Err(StatusCode::UNAUTHORIZED);
    };

    match argon2::verify_encoded(&hash_value, credentials.password.as_bytes()) {
        Ok(true) => (),
        _ => return Err(StatusCode::UNAUTHORIZED),
    };

    if hash_value != user.hash {
        return Err(StatusCode::UNAUTHORIZED);
    }

    // https://rust-lang-nursery.github.io/rust-cookbook/algorithms/randomness.html#create-random-passwords-from-a-set-of-alphanumeric-characters
    let random_api_key: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(30)
        .map(char::from)
        .collect();

    match sqlx::query!(
        "INSERT into sessions(user_name, api_key) values ($1, $2)",
        user.name,
        random_api_key
    )
    .execute(&mut transaction)
    .await
    {
        Ok(_) => (),
        Err(_) => return Err(StatusCode::INTERNAL_SERVER_ERROR),
    };

    use_api_key(&mut transaction, &random_api_key).await?;

    commit(transaction).await?;

    Ok(axum::Json(PostLoginResponse {
        api_key: random_api_key,
        access_level: level,
    }))
}

#[typeshare]
pub type PutLoginRequest = String;

pub async fn put_login(
    State(state): State<ApplicationState>,
    api_key: PutLoginRequest,
) -> Result<(), StatusCode> {
    let Ok(mut transaction) = state.pool.begin().await else {
        return Err(StatusCode::INTERNAL_SERVER_ERROR);
    };

    use_api_key(&mut transaction, &api_key).await?;

    commit(transaction).await
}
