use crate::{
    tables::{Material, UserAccessLevel},
    ApplicationState,
};
use axum::{extract::State, http::StatusCode};

use super::{
    auth::{verify, AuthenticatedQuery},
    db::commit,
};

#[typeshare::typeshare]
type GetMaterialsResponse = Vec<Material>;

pub async fn get_materials(
    State(state): State<ApplicationState>,
    axum::extract::Query(query): axum::extract::Query<AuthenticatedQuery>,
) -> Result<axum::Json<GetMaterialsResponse>, StatusCode> {
    let Ok(mut transaction) = state.pool.begin().await else {
        return Err(StatusCode::INTERNAL_SERVER_ERROR);
    };

    verify(&mut transaction, query.api_key, UserAccessLevel::Normal).await?;

    let materials = _get_materials(&mut transaction).await?;

    commit(transaction).await?;

    Ok(axum::Json(materials))
}

#[typeshare::typeshare]
type PostMaterialsRequest = Vec<Material>;

#[typeshare::typeshare]
type PostMaterialsResponse = PostMaterialsRequest;

pub async fn post_materials(
    State(state): State<ApplicationState>,
    axum::extract::Query(query): axum::extract::Query<AuthenticatedQuery>,
    axum::Json(materials): axum::Json<PostMaterialsRequest>,
) -> Result<axum::Json<PostMaterialsResponse>, StatusCode> {
    let Ok(mut transaction) = state.pool.begin().await else {
        return Err(StatusCode::INTERNAL_SERVER_ERROR);
    };

    verify(&mut transaction, query.api_key, UserAccessLevel::Admin).await?;

    for material in materials {
        match sqlx::query!(
            "INSERT INTO materials(vendor, material, color, owner) values ($1, $2, $3, $4)",
            material.vendor,
            material.material,
            material.color,
            material.owner,
        )
        .execute(&mut transaction)
        .await
        {
            Ok(_) => (),
            Err(_) => return Err(StatusCode::INTERNAL_SERVER_ERROR),
        };
    }

    let materials = _get_materials(&mut transaction).await?;

    commit(transaction).await?;

    Ok(axum::Json(materials))
}

#[typeshare::typeshare]
type DeleteMaterialsRequest = Vec<Material>;

#[typeshare::typeshare]
type DeleteMaterialsResponse = DeleteMaterialsRequest;

pub async fn delete_materials(
    State(state): State<ApplicationState>,
    axum::extract::Query(query): axum::extract::Query<AuthenticatedQuery>,
    axum::Json(materials): axum::Json<DeleteMaterialsRequest>,
) -> Result<axum::Json<DeleteMaterialsResponse>, StatusCode> {
    let Ok(mut transaction) = state.pool.begin().await else {
        return Err(StatusCode::INTERNAL_SERVER_ERROR);
    };

    verify(&mut transaction, query.api_key, UserAccessLevel::Admin).await?;

    for material in materials {
        match sqlx::query!(
            "DELETE FROM materials WHERE vendor=$1 AND material=$2 AND color=$3",
            material.vendor,
            material.material,
            material.color,
        )
        .execute(&mut transaction)
        .await
        {
            Ok(_) => (),
            Err(_) => return Err(StatusCode::INTERNAL_SERVER_ERROR),
        };
    }

    let materials = _get_materials(&mut transaction).await?;

    commit(transaction).await?;

    Ok(axum::Json(materials))
}

async fn _get_materials(
    transaction: &mut sqlx::Transaction<'_, sqlx::Postgres>,
) -> Result<Vec<Material>, StatusCode> {
    let Ok(materials) = sqlx::query_as!(Material, "SELECT * FROM materials")
        .fetch_all(transaction)
        .await else {
            return Err(StatusCode::INTERNAL_SERVER_ERROR);
        };

    Ok(materials)
}
