use axum::http::StatusCode;
use sqlx::types::chrono::Utc;
use tokio::time::sleep;

use crate::ApplicationState;

/// Update the field `last_used` of a session identified by the given `api_key`.
/// If the key does not exist clients will receive an http 440 error indicating that their session
/// has timed out and that they need to login again to acquire a new key.
/// This function does not commit its changes to the transaction this must be done
/// by the caller.
pub async fn use_api_key(
    transaction: &mut sqlx::Transaction<'_, sqlx::Postgres>,
    api_key: &String,
) -> Result<(), StatusCode> {
    let now = Utc::now().naive_utc();

    if sqlx::query!(
        "UPDATE sessions SET last_used=$1 WHERE api_key=$2 RETURNING api_key",
        now,
        api_key
    )
    .fetch_one(transaction)
    .await
    .is_ok()
    {
        return Ok(());
    }

    // 440 Login Time-out
    match StatusCode::from_u16(440) {
        Ok(status_code) => Err(status_code),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn prune_api_keys(state: ApplicationState) {
    loop {
        sleep(std::time::Duration::from_secs(10)).await;
        let Ok(mut transaction) = state.pool.begin().await else {
            println!("Failed to prune api keys, no connection to database");
            continue;
        };
        match sqlx::query!(
            "DELETE FROM sessions WHERE last_used IS NULL OR last_used < NOW() - INTERVAL '1 hours'"
        )
        .execute(&mut transaction)
        .await
        {
            Ok(_) => (),
            Err(_) => continue,
        };
        match transaction.commit().await {
            Ok(_) => (),
            Err(_) => continue,
        };
    }
}
