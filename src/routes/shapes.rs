use crate::{
    tables::{Shape, UserAccessLevel},
    ApplicationState,
};
use axum::{extract::State, http::StatusCode};

use super::{
    auth::{verify, AuthenticatedQuery},
    db::commit,
};

#[typeshare::typeshare]
type GetShapesResponse = Vec<Shape>;

pub async fn get_shapes(
    State(state): State<ApplicationState>,
    axum::extract::Query(query): axum::extract::Query<AuthenticatedQuery>,
) -> Result<axum::Json<GetShapesResponse>, StatusCode> {
    let Ok(mut transaction) = state.pool.begin().await else {
        return Err(StatusCode::INTERNAL_SERVER_ERROR);
    };

    verify(&mut transaction, query.api_key, UserAccessLevel::Normal).await?;

    let shapes = _get_shapes(&mut transaction).await?;

    commit(transaction).await?;

    Ok(axum::Json(shapes))
}

async fn _get_shapes(
    transaction: &mut sqlx::Transaction<'_, sqlx::Postgres>,
) -> Result<Vec<Shape>, StatusCode> {
    let Ok(shapes) = sqlx::query_as!(Shape, "SELECT * FROM shapes")
        .fetch_all(transaction)
        .await else {
            return Err(StatusCode::INTERNAL_SERVER_ERROR);
        };

    Ok(shapes)
}
