use std::str::FromStr;

use serde::{Deserialize, Serialize};
use sqlx::types::chrono::NaiveDateTime;
use typeshare::typeshare;

use crate::types::ApiKey;

#[derive(Debug, Serialize, Deserialize, sqlx::Type)]
#[typeshare]
pub struct Material {
    pub vendor: String,
    pub material: String,
    pub color: String,
    pub owner: String,
}

#[derive(Debug, Serialize, sqlx::Type)]
#[typeshare]
pub struct User {
    pub name: String,
    pub hash: String,
    pub salt: String,
}

#[derive(Debug, Serialize, PartialEq, sqlx::Type)]
#[serde(rename_all = "lowercase")]
#[sqlx(type_name = "level")]
#[sqlx(rename_all = "lowercase")]
#[typeshare]
pub enum UserAccessLevel {
    Normal,
    Admin,
}

impl FromStr for UserAccessLevel {
    type Err = ();

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "normal" => Ok(UserAccessLevel::Normal),
            "admin" => Ok(UserAccessLevel::Admin),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Serialize, sqlx::Type)]
#[typeshare]
pub struct AccessLevel {
    pub user_name: String,
    pub level: UserAccessLevel,
}

#[derive(Debug, sqlx::Type)]
pub struct Session {
    pub user_name: String,
    pub api_key: ApiKey,
    pub last_used: Option<NaiveDateTime>,
}

#[derive(Debug, Serialize, Deserialize)]
#[typeshare]
pub struct Shape {
    pub title: String,
    pub description: Option<String>,
    pub hash: String,
    pub path: String,
    pub public: bool,
    pub owner: String,
}
