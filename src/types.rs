use typeshare::typeshare;

#[typeshare]
pub type ApiKey = String;
